#ifndef IRON_C_H
#define IRON_C_H

#include "iron_obj.h"
#include "iron_lifetime.h"
#include "iron_fn.h"
#include "iron_class.h"

// Declares a local variable. Can be used for both classes and simple types, but classes must be
// declared this way. Initial value must be included.
#define var IRON_VAR

// Used to refer to a class type, can only be used within an iron function, variable, class, etc.
#define class IRON_CLASS

// Creates a new reference to a variable, generally used when returning it or sending it to
// function
#define ref IRON_REF

// Used to access a class property
#define acc IRON_ACC

// Used to assign a class instance
#define ass IRON_ASS

// Declares a function (only necessary if the function needs to be refered to before it's defined)
#define decl_fn IRON_FN_DECL

// Defines a function, followed by the funciton body
#define def_fn IRON_FN_DEF

// Defines a class, unlike function definitions the same class can appear in multiple compilation
// units (so this can go in header files)
#define def_class IRON_CLASS_DEF

// Defines a class's drop implementation, followed by the body
#define def_drop IRON_FN_DEF_DROP

// Defines a class's drop implementation with no behavior beyond the default
#define def_default_drop IRON_FN_DEF_DROP_DEFAULT

#endif // IRON_C_H
