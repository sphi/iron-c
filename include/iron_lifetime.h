#ifndef IRON_LIFETIME_H
#define IRON_LIFETIME_H

#include <string.h>
#include <stdlib.h>

struct unsafe_generic_class_t;

struct unsafe_lifetime_t {
    struct unsafe_generic_class_t* obj;
    struct unsafe_lifetime_t* next;
    struct unsafe_lifetime_t* prev;
};

// TODO: make these threadlocal and extern
static struct unsafe_lifetime_t unsafe_global_lifetime = {NULL, NULL, NULL};
static struct unsafe_lifetime_t* unsafe_current_lifetime = &unsafe_global_lifetime;

#define IRON_FN_SETUP_LOCAL_LIFETIME \
    struct unsafe_lifetime_t unsafe_lifetime_value = {NULL, NULL, NULL}; \
    struct unsafe_lifetime_t* unsafe_lifetime = &unsafe_lifetime_value; \
    memset(unsafe_lifetime, 0, sizeof(struct unsafe_lifetime_t)); \

static void unsafe_generic_ref(struct unsafe_generic_class_t* obj);
static void unsafe_generic_unref(struct unsafe_generic_class_t* obj);

static void unsafe_lifetime_insert_after(struct unsafe_lifetime_t* lifetime, struct unsafe_lifetime_t* target);

static struct unsafe_lifetime_t* unsafe_lifetime_new(struct unsafe_lifetime_t* target) {
    struct unsafe_lifetime_t* lifetime = malloc(sizeof(struct unsafe_lifetime_t));
    memset(lifetime, 0, sizeof(struct unsafe_lifetime_t));
    unsafe_lifetime_insert_after(lifetime, target);
    return lifetime;
}

static void unsafe_lifetime_drop_all(struct unsafe_lifetime_t* initial) {
    struct unsafe_lifetime_t* lifetime = initial;
    while (lifetime) {
        if (lifetime->obj) {
            unsafe_generic_unref(lifetime->obj);
            lifetime->obj = NULL;
        }
        struct unsafe_lifetime_t* to_free = lifetime;
        lifetime = lifetime->next;
        if (to_free != initial) {
            free(to_free);
        }
    }
}

static void unsafe_lifetime_extract(struct unsafe_lifetime_t* lifetime) {
    if (lifetime->prev) {
        lifetime->prev->next = lifetime->next;
    }
    if (lifetime->next) {
        lifetime->next->prev = lifetime->prev;
    }
    lifetime->next = NULL;
    lifetime->prev = NULL;
}

static void unsafe_lifetime_dispose(struct unsafe_lifetime_t* lifetime) {
    unsafe_lifetime_extract(lifetime);
    free(lifetime);
}

static void unsafe_lifetime_insert_after(struct unsafe_lifetime_t* lifetime, struct unsafe_lifetime_t* target) {
    if (target->next) {
        target->next->prev = lifetime;
    }
    lifetime->next = target->next;
    lifetime->prev = target;
    target->next = lifetime;
}

static void unsafe_lifetime_move_to(struct unsafe_lifetime_t* lifetime, struct unsafe_lifetime_t* target) {
    unsafe_lifetime_extract(lifetime);
    unsafe_lifetime_insert_after(lifetime, target);
}

#endif // IRON_LIFETIME_H
