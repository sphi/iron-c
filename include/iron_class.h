#ifndef IRON_CLASS_H
#define IRON_CLASS_H

#include <stdlib.h>
#include "iron_obj.h"
#include "iron_lifetime.h"
#include "iron_fn.h"

struct unsafe_generic_class_t;

struct unsafe_generic_vtable_t {
    void (*drop)(struct unsafe_generic_class_t*, struct unsafe_lifetime_t*);
    void (*for_each_obj)(struct unsafe_generic_class_t*, void (*)(struct unsafe_generic_class_t*, void*), void*);
};

struct unsafe_generic_class_t {
    struct unsafe_generic_vtable_t* vtable;
    int ref_count;
};

static void unsafe_generic_ref(struct unsafe_generic_class_t* obj) {
    obj->ref_count++;
}

static void unsafe_generic_unref(struct unsafe_generic_class_t* obj);
static void unsafe_generic_unref_with_data(struct unsafe_generic_class_t* obj, void* data_unused)
{
    (void)data_unused;
    unsafe_generic_unref(obj);
}
static void unsafe_generic_unref(struct unsafe_generic_class_t* obj) {
    obj->ref_count--;
    if (obj->ref_count == 0) {
        if (obj->vtable->drop) {
            IRON_FN_SETUP_LOCAL_LIFETIME;
            obj->vtable->drop(obj, unsafe_lifetime);
            unsafe_lifetime_drop_all(unsafe_lifetime);
        }
        obj->vtable->for_each_obj(obj, unsafe_generic_unref_with_data, NULL);
        free(obj);
    }
}

#define IRON_MANGLE_DROP_WRAPPER_FN(name) name##_unsafe_drop
#define IRON_MANGLE_DROP_BODY_FN(name) name##_unsafe_drop_inner
#define IRON_MANGLE_FOR_EACH_FN(name) name##_unsafe_for_each
#define IRON_MANGLE_MAKE_TETHER_FN(name) name##_unsafe_make_tether

#define IRON_CLASS_MEMBER_DECL(member, index) \
    EVIL_IF(EVIL_IS_THING(member)) \
        (IRON_EXTRACT_ARG(member));

#define IRON_CLASS_CONSTRUCTOR_ARG_DEF(member, index) \
    EVIL_IF(EVIL_IS_THING(member)) ( \
        EVIL_IF_NOT(EVIL_EQ(index, 0)) \
            (,) \
        IRON_EXTRACT_BOTH(member))

#define IRON_CLASS_CONSTRUCTOR_ARG_INIT(member, index) \
    EVIL_IF(EVIL_IS_THING(member)) \
        (EVIL_IF_ELSE(IRON_IS_OBJ(member)) \
            (self.unsafe_obj->unsafe_props.EVIL_EXPAND_CALL(IRON_MANGLE_NAME, IRON_EXTRACT_NAME(member)) \
                = IRON_EXTRACT_NAME(member); \
             unsafe_lifetime_dispose(IRON_EXTRACT_NAME(member).unsafe_lifetime);) \
            (self.unsafe_obj->unsafe_props.IRON_EXTRACT_NAME(member) = IRON_EXTRACT_NAME(member);))

#define IRON_CLASS_FOR_EACH_CALL(type_name, member, index) \
    EVIL_IF(EVIL_IS_THING(member)) \
        (EVIL_IF(IRON_IS_OBJ(member)) \
            (callback( \
                &((struct IRON_MANGLE_TYPE(type_name)*)self)->unsafe_props \
                    .EVIL_EXPAND_CALL(IRON_MANGLE_NAME, IRON_EXTRACT_NAME(member)) \
                    .unsafe_obj->unsafe_info, \
                data);))

#define IRON_CLASS_DEF(name, ...) \
    struct IRON_MANGLE_TYPE(name); \
    struct IRON_MANGLE_TETHER_TYPE(name) { \
        struct IRON_MANGLE_TYPE(name)* unsafe_obj; \
        struct unsafe_lifetime_t* unsafe_lifetime; \
    }; \
    struct IRON_MANGLE_LOCKED_TETHER_TYPE(name) { \
        struct IRON_MANGLE_TYPE(name)* unsafe_obj; \
    }; \
    struct IRON_MANGLE_TYPE(name) { \
        struct unsafe_generic_class_t unsafe_info; \
        struct IRON_MANGLE_TETHER_TYPE(name)(*unsafe_make_tether)( \
            struct IRON_MANGLE_TYPE(name)*, \
            struct unsafe_lifetime_t*) __attribute__((warn_unused_result)); \
        /* putting properties in inner structure prevents users getting access to object pointers */ \
        struct { \
            EVIL_MAP((IRON_CLASS_MEMBER_DECL), __VA_ARGS__) \
        } unsafe_props; \
    }; \
    extern struct unsafe_generic_vtable_t IRON_MANGLE_VTABLE(name); \
    struct IRON_MANGLE_TETHER_TYPE(name) IRON_MANGLE_MAKE_TETHER_FN(name)( \
        struct IRON_MANGLE_TYPE(name)*, \
        struct unsafe_lifetime_t*); \
    static struct IRON_MANGLE_TETHER_TYPE(name) new_##name( \
        EVIL_MAP((IRON_CLASS_CONSTRUCTOR_ARG_DEF), __VA_ARGS__) \
    ) __attribute__((warn_unused_result)); \
    static struct IRON_MANGLE_TETHER_TYPE(name) new_##name( \
        EVIL_MAP((IRON_CLASS_CONSTRUCTOR_ARG_DEF), __VA_ARGS__) \
    ) { \
        struct IRON_MANGLE_TETHER_TYPE(name) self; \
        self.unsafe_obj = malloc(sizeof(struct IRON_MANGLE_TYPE(name))); \
        memset(self.unsafe_obj, 0, sizeof(struct IRON_MANGLE_TYPE(name))); \
        /* TODO: assert malloc did not fail */ \
        self.unsafe_obj->unsafe_info.vtable = &IRON_MANGLE_VTABLE(name); \
        self.unsafe_obj->unsafe_info.ref_count = 1; \
        self.unsafe_obj->unsafe_make_tether = IRON_MANGLE_MAKE_TETHER_FN(name); \
        EVIL_MAP((IRON_CLASS_CONSTRUCTOR_ARG_INIT), __VA_ARGS__) \
        self.unsafe_lifetime = unsafe_lifetime_new(unsafe_current_lifetime); \
        self.unsafe_lifetime->obj = &self.unsafe_obj->unsafe_info; \
        return self; \
    } \
    static void IRON_MANGLE_FOR_EACH_FN(name) ( \
        struct unsafe_generic_class_t* self, \
        void (*callback)(struct unsafe_generic_class_t*, void*), \
        void* data \
    ) {\
        EVIL_MAP((IRON_CLASS_FOR_EACH_CALL, name), __VA_ARGS__) \
    }

#define IRON_FN_DROP_BODY_DECL(name) \
    static void IRON_MANGLE_DROP_BODY_FN(name)(\
        struct IRON_MANGLE_LOCKED_TETHER_TYPE(name) IRON_MANGLE_NAME(self), \
        struct unsafe_lifetime_t* lifetime) \

#define IRON_MAKE_TETHER_DEF(name) \
    struct IRON_MANGLE_TETHER_TYPE(name) IRON_MANGLE_MAKE_TETHER_FN(name)( \
        struct IRON_MANGLE_TYPE(name)* obj, \
        struct unsafe_lifetime_t* lifetime \
    ) { \
        lifetime->obj = &obj->unsafe_info; \
        unsafe_generic_ref(&obj->unsafe_info); \
        return (struct IRON_MANGLE_TETHER_TYPE(name)){obj, lifetime}; \
    }

#define IRON_FN_DEF_DROP(name) \
    IRON_MAKE_TETHER_DEF(name) \
    IRON_FN_DROP_BODY_DECL(name); \
    void IRON_MANGLE_DROP_WRAPPER_FN(name)(struct unsafe_generic_class_t* self, struct unsafe_lifetime_t* lifetime) { \
        IRON_MANGLE_DROP_BODY_FN(name)( \
            (struct IRON_MANGLE_LOCKED_TETHER_TYPE(name)){(struct IRON_MANGLE_TYPE(name)*)self}, \
            lifetime); \
    } \
    struct unsafe_generic_vtable_t IRON_MANGLE_VTABLE(name) = { \
        IRON_MANGLE_DROP_WRAPPER_FN(name), \
        IRON_MANGLE_FOR_EACH_FN(name), \
    }; \
    IRON_FN_DROP_BODY_DECL(name)

#define IRON_FN_DEF_DROP_DEFAULT(name) \
    IRON_MAKE_TETHER_DEF(name) \
    struct unsafe_generic_vtable_t IRON_MANGLE_VTABLE(name) = { \
        NULL, \
        IRON_MANGLE_FOR_EACH_FN(name), \
    };

#endif // IRON_CLASS_H
