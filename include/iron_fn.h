#ifndef IRON_FN_H
#define IRON_FN_H

#include "iron_obj.h"
#include "iron_lifetime.h"

#define IRON_UNCHECKED_EQ(a_val, b_val) \
    EVIL_NOT(EVIL_IS_THING(EVIL_EXPAND_CAT(EVIL_ENABLE_EQ_, EVIL_EXPAND_CAT(a_val, EVIL_EXPAND_CAT(_, b_val)))))

#define IRON_HANDLE_FN_ARG_WRAPPER(arg, index) \
    EVIL_IF_NOT(EVIL_EQ(index, 0)) (,) \
    IRON_EXTRACT_BOTH(arg)
#define IRON_ARG_LIST_WRAPPER(...) \
    EVIL_MAP((IRON_HANDLE_FN_ARG_WRAPPER), __VA_ARGS__)

// A trailing comma only if there is an argument
#define IRON_HANDLE_FN_ARG_BODY(arg, index) \
    IRON_EXTRACT_ARG(arg),
#define IRON_ARG_LIST_BODY(...) \
    EVIL_MAP((IRON_HANDLE_FN_ARG_BODY), __VA_ARGS__)

#define IRON_HANDLE_ARG_NAME(arg, index) \
    IRON_EXTRACT_NAME(arg),
#define IRON_ARG_NAME_LIST(...) \
    EVIL_MAP((IRON_HANDLE_ARG_NAME), __VA_ARGS__)

#define IRON_FN_WRAPPER_DECL(ret_type_and_name, arg_list) \
    IRON_EXTRACT_BOTH(ret_type_and_name) (IRON_ARG_LIST_WRAPPER arg_list)

#define IRON_FN_BODY_NAME(name) EVIL_EXPAND_CAT(name, _unsafe_inner)

#define IRON_FN_BODY_DECL(ret_type_and_name, arg_list) \
    static IRON_EXTRACT_TYPE(ret_type_and_name) IRON_FN_BODY_NAME(IRON_EXTRACT_NAME(ret_type_and_name)) \
    (IRON_ARG_LIST_BODY arg_list struct unsafe_lifetime_t* unsafe_lifetime)

#define IRON_FN_ARG_INIT(member, index) \
    EVIL_IF(IRON_IS_OBJ(member)) \
        (unsafe_lifetime_move_to( \
            IRON_EXTRACT_NAME(member).unsafe_lifetime, \
            unsafe_lifetime);)
#define IRON_FN_ARGs_INIT(...) \
    EVIL_MAP((IRON_FN_ARG_INIT), __VA_ARGS__)

#define IRON_FN_DEF(ret_type_and_name, arg_list) \
    IRON_FN_BODY_DECL(ret_type_and_name, arg_list); \
    IRON_FN_WRAPPER_DECL(ret_type_and_name, arg_list) { \
        IRON_FN_SETUP_LOCAL_LIFETIME \
        struct unsafe_lifetime_t* calling_lifetime = unsafe_current_lifetime; \
        unsafe_current_lifetime = unsafe_lifetime; \
        IRON_FN_ARGs_INIT arg_list \
        EVIL_IF(EVIL_NOT(IRON_UNCHECKED_EQ(void, IRON_EXTRACT_TYPE(ret_type_and_name)))) \
            (IRON_EXTRACT_TYPE(ret_type_and_name) unsafe_result =) \
        IRON_FN_BODY_NAME(IRON_EXTRACT_NAME(ret_type_and_name))(IRON_ARG_NAME_LIST arg_list unsafe_lifetime); \
        EVIL_IF(IRON_IS_OBJ(ret_type_and_name)) \
            (unsafe_lifetime_move_to(unsafe_result.unsafe_lifetime, calling_lifetime);) \
        unsafe_current_lifetime = calling_lifetime; \
        unsafe_lifetime_drop_all(unsafe_lifetime); \
        EVIL_IF(EVIL_NOT(IRON_UNCHECKED_EQ(void, IRON_EXTRACT_TYPE(ret_type_and_name)))) \
            (return unsafe_result;) \
    } \
    IRON_FN_BODY_DECL(ret_type_and_name, arg_list)

#define IRON_FN_DECL IRON_FN_WRAPPER_DECL

#endif // IRON_FN_H
