#ifndef IRON_OBJ_H
#define IRON_OBJ_H

// This file contains the macros for creating and accessing class instances

#include "define_EVIL.h"

// Turns normal names into unsafe names which will be what's given to the compiler
// The struct that holds the actual class data
#define IRON_MANGLE_TYPE(type) type##_unsafe_t
// Variable name for a class's vtable
#define IRON_MANGLE_VTABLE(type) type##_unsafe_vtable
// The struct that holds a pointer to the class, and to a lifetime; generally passed around by value
#define IRON_MANGLE_TETHER_TYPE(type) type##_unsafe_tether_t
// Like a normal tether, except doesn't have a lifetime and so can not be set or refed away
#define IRON_MANGLE_LOCKED_TETHER_TYPE(type) type##_unsafe_locked_teather_t
// A variable name for an object
#define IRON_MANGLE_NAME(name) name##_unsafe

#define IRON_APPEND_COMMA(item) item,
#define IRON_GET_FIRST(first, second) first
#define IRON_GET_SECOND(first, second) second

// (int) foo            -> int
// (class xyz) foo      -> xyz
#define IRON_EXTRACT_TYPE_NAME(bundle) \
    EVIL_EXPAND_CALL(IRON_GET_FIRST, IRON_APPEND_COMMA bundle)

// (int) foo            -> int
// (class xyz) foo      -> struct xyz_unsafe_tether_t
#define IRON_EXTRACT_TYPE(bundle) \
    EVIL_IF_ELSE(IRON_IS_OBJ(bundle)) \
        (struct EVIL_EXPAND_CALL(IRON_MANGLE_TETHER_TYPE, IRON_EXTRACT_TYPE_NAME(bundle))) \
        (IRON_EXTRACT_TYPE_NAME(bundle))

// (int) foo            -> foo
// (class xyz) foo      -> foo
#define IRON_EXTRACT_NAME(bundle) \
    EVIL_IF_ELSE(IRON_IS_OBJ(bundle)) \
        (EVIL_EXPAND_CAT(VANQUISH_, EVIL_EXPAND_CALL(IRON_GET_SECOND, IRON_APPEND_COMMA bundle))) \
        (EVIL_EXPAND_CALL(IRON_GET_SECOND, IRON_APPEND_COMMA bundle))

// (int) foo            -> int foo
// (class xyz) foo      -> struct xyz_unsafe_tether_t foo
#define IRON_EXTRACT_BOTH(bundle) \
    IRON_EXTRACT_TYPE(bundle) IRON_EXTRACT_NAME(bundle)

// Like IRON_EXTRACT_BOTH, except mangles the name for safe access
// (int) foo            -> int foo
// (class xyz) foo      -> struct xyz_unsafe_tether_t foo_unsafe
#define IRON_EXTRACT_ARG(bundle) \
    IRON_EXTRACT_TYPE(bundle) \
    EVIL_IF_ELSE(IRON_IS_OBJ(bundle)) \
        (EVIL_EXPAND_CALL(IRON_MANGLE_NAME, IRON_EXTRACT_NAME(bundle))) \
        (IRON_EXTRACT_NAME(bundle))

#define VANQUISH_IRON_OBJ_MARKER
#define COMMA_FROM_IRON_OBJ_MARKER ,

// (int) foo            -> FALSE
// (class xyz) foo      -> TRUE
#define IRON_IS_OBJ(bundle) \
    EVIL_EQ( \
        2, \
        EVIL_COUNT_AT_LEAST_1(EVIL_EXPAND_CAT( \
            COMMA_FROM_, \
            EVIL_EXPAND_CALL(IRON_GET_SECOND, IRON_APPEND_COMMA bundle))))

#define IRON_CLASS_INNER(type) \
    type \
    ) /* matched in IRON_CLASS */ \
    IRON_OBJ_MARKER
#define IRON_CLASS IRON_CLASS_INNER(

#define IRON_DRILL_CLASS_ITEM(unmangled, index) \
    ->unsafe_props.IRON_MANGLE_NAME(unmangled).unsafe_obj
#define IRON_DRILL_CLASS(first, ...) \
    IRON_MANGLE_NAME(first).unsafe_obj EVIL_MAP((IRON_DRILL_CLASS_ITEM), __VA_ARGS__)

#define IRON_REF_INNER(first, ...) \
    IRON_DRILL_CLASS(first, __VA_ARGS__)->unsafe_make_tether( \
        IRON_DRILL_CLASS(first, __VA_ARGS__), \
        unsafe_lifetime_new(unsafe_lifetime)) \
    ) /* matched in IRON_REF */
#define IRON_REF IRON_REF_INNER(

// Declare a local variable that holds an object
#define IRON_VAR(type_and_name, value) \
    IRON_EXTRACT_ARG(type_and_name) = value; \
    EVIL_IF(IRON_IS_OBJ(type_and_name)) \
        (unsafe_lifetime_move_to( \
            EVIL_EXPAND_CALL(IRON_MANGLE_NAME, IRON_EXTRACT_NAME(type_and_name)).unsafe_lifetime, \
            unsafe_lifetime))

#define IRON_ACC_INNER(first, ...) \
    (IRON_DRILL_CLASS(first, __VA_ARGS__)->unsafe_props) \
    ) /* matched in IRON_ACC */
#define IRON_ACC IRON_ACC_INNER(

// TODO: update IRON_ASS to use raw pointers in members instead of tethers

// Assigns the value of an object
#define IRON_ASS_DEST_ITEM(unmangled, index) \
    EVIL_IF_NOT(EVIL_EQ(index, 0)) \
        (.unsafe_obj->unsafe_props.IRON_MANGLE_NAME(unmangled))
#define IRON_ASS_DEST(first, ...) \
    IRON_MANGLE_NAME(first)EVIL_MAP_DOWN((IRON_ASS_DEST_ITEM), __VA_ARGS__)
#define IRON_ASS_DEST_ITEM_PARENT(unmangled, index) \
    EVIL_IF_NOT(EVIL_OR(EVIL_EQ(index, 0), EVIL_EQ(index, 1))) ( \
        IRON_MANGLE_NAME(unmangled). /* note the trailing dot, unlike above version */ \
        EVIL_IF_NOT(EVIL_EQ(index, 2)) ( \
            /* thus no leading dot here */ unsafe_obj->unsafe_props.))
#define IRON_ASS_SOURCE_ITEM(item, index) \
    EVIL_IF(EVIL_EQ(index, 0)) (item)
#define IRON_ASS(first, ...) \
    { \
        struct unsafe_generic_class_t* unsafe_obj_to_unref = \
            &IRON_ASS_DEST(first, __VA_ARGS__).unsafe_obj->unsafe_info; \
        EVIL_IF(EVIL_EQ(EVIL_COUNT_AT_LEAST_1(__VA_ARGS__), 1)) \
            (unsafe_lifetime_dispose(IRON_MANGLE_NAME(first).unsafe_lifetime);) \
        IRON_ASS_DEST(first, __VA_ARGS__) = EVIL_MAP_DOWN((IRON_ASS_SOURCE_ITEM), first, __VA_ARGS__); \
        EVIL_IF_ELSE(EVIL_EQ(EVIL_COUNT_AT_LEAST_1(__VA_ARGS__), 1)) \
            (unsafe_lifetime_move_to(IRON_ASS_DEST(first, __VA_ARGS__).unsafe_lifetime, unsafe_lifetime);) \
            (unsafe_lifetime_dispose(IRON_ASS_DEST(first, __VA_ARGS__).unsafe_lifetime); \
             IRON_ASS_DEST(first, __VA_ARGS__).unsafe_lifetime = NULL;) \
        unsafe_generic_unref(unsafe_obj_to_unref); \
    }

#endif // IRON_OBJ_H
