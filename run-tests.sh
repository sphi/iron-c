#!/bin/bash
set -euo pipefail

# Move into the directory of the script
cd "$( dirname "${BASH_SOURCE[0]}" )"

TEST_FLAGS="$(cat flags.txt)"
COMPILERS="clang gcc"

COLOR_GREEN='\x1b[32;1m'
COLOR_RED='\x1b[31;1m'
COLOR_NONE='\x1b[0m'

PASSED_TESTS=0
FAILED_TESTS=0
FAILED_TEST_NAMES=""

BUILD_DIR="test/build"
rm -Rf "$BUILD_DIR"
mkdir -p "$BUILD_DIR"

# Hack for running from Termux on Android where only files in the Termux data dir can be executable
if test ! -z "${TERMUX_VERSION:-}"
then
  BUILD_DIR="/data/data/com.termux/files/home/iron-c-build"
fi

# Vars that need to be set:
#   TEST_IN_FILE: input .c file path
#   TEST_OUT_FILE: output binary path
#   TEST_SHOULD_ERROR: 1 if test is expected to fail to build
#   TEST_CC: compiler
#   TEST_FLAGS: compiler flags
# Sets:
#   TEST_RESULT: empty if there is an error
build_test() {
    rm -f "$TEST_OUT_FILE"
    COMPILER_CMD="$TEST_CC -g $TEST_FLAGS $TEST_IN_FILE -o $TEST_OUT_FILE"
    COMPILER_OUTPUT=$($COMPILER_CMD -fdiagnostics-color=always 2>&1 || true)
    TEST_RESULT=""
    if test ! -f "$TEST_OUT_FILE" -a "$TEST_SHOULD_ERROR" -eq 0
    then
        TEST_RESULT="$COMPILER_CMD\n${COLOR_RED}failed to build:$COLOR_NONE $COMPILER_OUTPUT"
    elif test -f "$TEST_OUT_FILE" -a "$TEST_SHOULD_ERROR" -ne 0
    then
        TEST_RESULT="$COMPILER_CMD\n${COLOR_RED}expected to error, but built successfully$COLOR_NONE"
    fi
}

# Vars that need to be set:
#   TEST_IN_FILE: input .c file path
#   TEST_OUT_FILE: output binary path
#   TEST_SHOULD_FAIL: 1 if test is expected to produce wrong output
# Sets:
#   TEST_RESULT: empty if there is an error
run_test() {
    EXPECTED_STDOUT=$(grep -P '// STDOUT:.*' "$TEST_IN_FILE" | sed 's~// STDOUT:\s*~~g' || true)
    STDOUT=$($TEST_OUT_FILE 2>/dev/null || true)
    STDERR=$($TEST_OUT_FILE 2>&1 1>/dev/null || true)
    EXIT_CODE=$($TEST_OUT_FILE 2>/dev/null 1>/dev/null; echo $?)
    if test "$EXIT_CODE" -ne 0
    then
        TEST_RESULT="$TEST_OUT_FILE\n${COLOR_RED}failed with exit code $EXIT_CODE:$COLOR_NONE $STDERR"
    elif test ! -z "$STDERR"
    then
        TEST_RESULT="$TEST_OUT_FILE\n${COLOR_RED}produced stderr:$COLOR_NONE $STDERR"
    elif test "$STDOUT" != "$EXPECTED_STDOUT"
    then
        TEST_RESULT="$TEST_OUT_FILE\n${COLOR_RED}did not produce expected output:$COLOR_NONE\n${COLOR_GREEN}expected:$COLOR_NONE $EXPECTED_STDOUT\n${COLOR_RED}actual:$COLOR_NONE   $STDOUT"
    fi
    if test "$TEST_SHOULD_FAIL" -ne 0
    then
        if test "$EXIT_CODE" -eq 0
        then
            TEST_RESULT="$TEST_OUT_FILE\n${COLOR_RED}expected to fail at runtime, but succeeded$COLOR_NONE"
        else
            TEST_RESULT=""
        fi
    fi
}

perform_test() {
    TEST_IN_FILE="$1"
    TEST_NAME="$(echo "$TEST_IN_FILE" | sed 's:test/::g;s:\btest_::g;s:\.c$::')"
    TEST_SHOULD_ERROR=$(echo "$TEST_NAME" | grep -P '_errors$' >/dev/null && echo 1 || echo 0)
    TEST_SHOULD_FAIL=$(echo "$TEST_NAME" | grep -P '_fails$' >/dev/null && echo 1 || echo 0)
    for TEST_CC in $COMPILERS; do
        TEST_OUT_FILE="$BUILD_DIR/$(echo $TEST_NAME | sed 's:/:_:g')_$TEST_CC"
        build_test
        if test ! -z "$TEST_RESULT"
        then
            break
        fi
        if test $TEST_SHOULD_ERROR -eq 0
        then
            run_test
            if test ! -z "$TEST_RESULT"
            then
                break
            fi
        fi
    done
    if test -z "$TEST_RESULT"
    then
        printf "    $COLOR_GREEN.$COLOR_NONE $TEST_NAME\n"
        ((PASSED_TESTS+=1))
    else
        printf "    ${COLOR_RED}X$COLOR_NONE $TEST_NAME:\n$TEST_RESULT\n\n"
        ((FAILED_TESTS+=1))
        FAILED_TEST_NAMES="$FAILED_TEST_NAMES\n  $TEST_NAME"
    fi
}

# Loop through all test files
for test in $(find "test" -name 'test_*.c' -not -name '*.expanded.c')
do
    perform_test "$test"
done

echo
if test $FAILED_TESTS -eq 0
then
    printf "${COLOR_GREEN}all $PASSED_TESTS tests passed!$COLOR_NONE\n"
    exit 0
else
    printf "${COLOR_RED}$PASSED_TESTS tests passed and $FAILED_TESTS failed:$COLOR_NONE$FAILED_TEST_NAMES\n"
    exit 1
fi
