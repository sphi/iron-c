#!/bin/bash
set -euo pipefail

# Location of the script is the project root
ROOT="$(realpath "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")"

if test "$#" -ne 1
then
    echo "Should be given one argument"
    exit 1
fi

SOURCE="$1"

if test ! -f "$SOURCE"
then
    echo "$SOURCE does not exist"
    exit 1
fi

FLAGS="$(cat "$ROOT/flags.txt")"
CC="clang"

OUT_FILE="$SOURCE.expanded.c"
echo "Generating $OUT_FILE..."
rm -f "$OUT_FILE"
COMPILER_CMD="$CC $FLAGS $SOURCE -E -o $OUT_FILE"
echo "> $COMPILER_CMD"
COMPILER_OUTPUT=$($COMPILER_CMD -fdiagnostics-color=always 2>&1 || true)
if ! test -f "$OUT_FILE"
then
    echo "$COMPILER_OUTPUT"
    exit 1
fi
sed -i 's:#\s*[0-9]\+\s*":// \0:g' "$OUT_FILE"
echo "Success! Formatting..."
clang-format -i "$OUT_FILE"
echo "Success! Compiling..."
COMPILER_CMD="$CC -g $FLAGS $OUT_FILE"
echo "> $COMPILER_CMD"
$COMPILER_CMD
