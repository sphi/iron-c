#include <stdio.h>
#include "iron_c.h"

// STDOUT: foo
// STDOUT: 7
// STDOUT: 22 x bar

decl_fn( (int) get_num, () );

decl_fn( (void) print_abc, (
    (int) a, (char) b, (const char *) c
) );

def_fn( (int) main, () ) {
    printf("foo\n");
    int result = get_num();
    printf("%d\n", result);
    print_abc(22, 'x', "bar");
    return 0;
}

def_fn( (int) get_num, () ) {
    return 7;
}

def_fn( (void) print_abc, (
    (int) a, (char) b, (const char *) c
) ) {
    printf("%d %c %s\n", a, b, c);
}
