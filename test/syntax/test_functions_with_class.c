#include <stdio.h>
#include "iron_c.h"

// STDOUT: 5, 7

def_class( point,
    (int) x,
    (int) y,
);

def_default_drop( point );

def_fn( (class point) point_origin, () ) {
    return new_point(0, 0);
}

def_fn( (void) point_add, (
    (class point) p, (class point) other
)) {
    (acc p).x += (acc other).x;
    (acc p).y += (acc other).y;
}

def_fn( (void) point_print, (
    (class point) p
)) {
    printf("%d, %d\n", (acc p).x, (acc p).y);
}

def_fn( (int) main, () ) {
    var( (class point) p, point_origin() );
    point_add((ref p), new_point(2, 2));
    point_add((ref p), new_point(3, 5));
    point_print((ref p));
    return 0;
}
