#include <stdio.h>
#include "iron_c.h"

// STDOUT: bar baz foo

def_class( contained,
    (const char*) text,
);
def_default_drop( contained );

def_class( outer,
    (class contained) prop,
);
def_default_drop( outer );

def_fn( (int) main, () ) {
    var( (class outer) a, new_outer(new_contained("foo")) );
    var( (class outer) b, new_outer(new_contained("bar")) );
    var( (class contained) c, (ref a,prop) );
    ass( a,prop, (ref b,prop) );
    ass( b,prop, new_contained("baz") );
    printf("%s %s %s\n", (acc a,prop).text, (acc b,prop).text, (acc c).text);
    return 0;
}
