#include <stdio.h>
#include "iron_c.h"

// STDOUT: 7 xyz

def_class( my_class,
    (int) num,
    (const char*) text,
);

def_default_drop( my_class );

def_fn( (int) main, () ) {
    var( (class my_class) foo, new_my_class(7, "xyz") );
    printf("%d %s\n", (acc foo).num, (acc foo).text);
    return 0;
}
