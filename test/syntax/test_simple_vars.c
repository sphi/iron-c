#include <stdio.h>
#include "iron_c.h"

// STDOUT: foo 7

def_fn( (int) main, () ) {
    var( (const char*) a, "foo" );
    var( (int) b, 2 );
    b = 7;
    printf("%s %d", a, b);
    return 0;
}

