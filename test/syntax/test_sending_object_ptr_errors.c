#include <stdio.h>
#include "iron_c.h"

// STDOUT: 5, 7

def_class(foo,
    (int) a,
    (int) b,
);

def_default_drop(foo);

def_fn( (void) take_foo_ptr, ((class foo)* ptr) ) {}

def_fn((int) main, ()) {
    var( (class foo) p, new_foo(1, 2) );
    take_foo_ptr(&(ref p));
    return 0;
}
