#include <stdio.h>
#include "iron_c.h"

// STDOUT: arg1 dropped
// STDOUT: foo B dropped
// STDOUT: foo A dropped
// STDOUT: arg replacement dropped
// STDOUT: arg2 dropped
// STDOUT: foo result dropped
// STDOUT: result replacement dropped

// Defines a "node" class with a single property
def_class( node,
    (const char*) name,
);

// When dropped, a node will print it's name
def_drop( node ) {
    printf("%s dropped\n", (acc self).name);
}

// Defines a "foo" function that takes two "node" arguments
def_fn( (class node) foo, (
    (class node) arg1, (class node) arg2
) ) {
    // Assigns a new node to the first argument, this causes arg1 to be dropped
    ass( arg1, new_node("arg replacement") );
    // Creates three local variables and three new nodes they're set to
    var( (class node) a, new_node("foo A") );
    var( (class node) result, new_node("foo result") );
    var( (class node) b, new_node("foo B") );
    // Returns a strong ref to result, which will prevent result from being dropped
    return (ref result);
    // "foo A", "foo B", "arg replacement" and "arg2" are dropped here
}

def_fn( (int) main, () ) {
    // Creates two new nodes and sends them to foo, stores the result
    var( (class node) result, foo(new_node("arg1"), new_node("arg2")) );
    // Replaces the result of foo with a new node, this causes the result to be dropped
    ass( result, new_node("result replacement") );
    // On exit, the remaining nodes are dropped
    return 0;
}
