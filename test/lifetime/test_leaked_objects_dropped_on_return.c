#include <stdio.h>
#include "iron_c.h"

// STDOUT: node leaked
// STDOUT: node dropped
// STDOUT: foo done

def_class( node,
);

def_drop( node ) {
    printf("node dropped\n");
}

def_fn( (void) foo, () ) {
    // This leaks the object, but it should still be dropped when foo returns
    struct node_unsafe_tether_t leaked = new_node();
    printf("node leaked\n");
}

def_fn( (int) main, () ) {
    foo();
    printf("foo done\n");
    return 0;
}
