#include <stdio.h>
#include "iron_c.h"

// STDOUT: Hello World!
// STDOUT: line 2

int main() {
    printf("Hello World!\n");
    printf("line 2\n");
    return 0;
}
